FROM ubuntu:16.04

RUN apt-get update && apt-get install -y openssh-server
RUN mkdir /var/run/sshd
RUN echo 'root:connect$6' | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]

#RUN apt-get update && apt-get install -y python3.5 python3-pip

#CWB main package
RUN cd /tmp && wget https://iweb.dl.sourceforge.net/project/cwb/cwb/cwb-3.0.0/cwb-3.0.0-linux-x86_64.tar.gz
RUN cd /tmp && tar xvfz cwb-3.0.0-linux-x86_64.tar.gz
RUN cd /tmp && cd cwb-3.0.0-linux-x86_64/ &&  ./install-cwb.sh


#Perl base package, recommended for all CWB users 
RUN apt-get update && apt-get install -y libhtml-parser-perl && apt-get install -y make
RUN cd /tmp && wget http://cwb.sourceforge.net/temp/Perl-CWB-2.2.102.tar.gz
RUN cd /tmp && tar xvfz Perl-CWB-2.2.102.tar.gz
RUN cd /tmp && cd CWB-2.2.102/ && perl Makefile.PL && make && make test && make install


#Perl low-level corpus access (requires C compiler)
RUN cd /tmp && wget http://cwb.sourceforge.net/temp/Perl-CWB-CL-2.2.102.tar.gz
RUN apt-get -y install build-essential libssl-dev libffi-dev python-dev libpq-dev
RUN cd /tmp && tar xvfz Perl-CWB-CL-2.2.102.tar.gz
RUN cd /tmp && cd CWB-CL-2.2.102/ && perl Makefile.PL && make && make test && make install


#Perl support package for Web GUIs
RUN cd /tmp && wget http://cwb.sourceforge.net/temp/Perl-CWB-Web-2.2.102.tar.gz
RUN cd /tmp && tar xvfz Perl-CWB-Web-2.2.102.tar.gz
RUN cd /tmp && cd CWB-Web-2.2.102/ && perl Makefile.PL && make && make test && make install

#Perl CQi reference implementation
RUN cd /tmp && wget http://cwb.sourceforge.net/temp/Perl-CWB-CQI-2.2.102.tar.gz
RUN cd /tmp && tar xvfz Perl-CWB-CQI-2.2.102.tar.gz
RUN cd /tmp && cd CWB-CQI-2.2.102/ && perl Makefile.PL && make && make test && make install

#Python stuff
RUN apt-get -y install python3-pip
RUN pip3 --version
RUN pip3 install --upgrade pip
ENV PYTHONUNBUFFERED 1
RUN mkdir /web
WORKDIR /web
COPY requirements.txt /web/
RUN pip3 install -r requirements.txt
COPY . /web/